import { A } from '@ember/array';
import EmberObject from '@ember/object';

export default EmberObject.extend({
  init: function(){
    this._super();
    this.set('list', A());
  },
  getList: function(){
    return this.get('list');
  },
  addObject: function (item) {
    this.get('list').addObject(item);
  },
  removeObject: function (item) {
    this.getList().removeObject(item);
  },
  loadArray: function (a) {
     a.forEach(function (e) {
      this.addObject(e);
    }.bind(this));
  }
  // add other mutators here that change the state of the data
});
