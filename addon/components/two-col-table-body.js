import Component from '@ember/component';
import TwoColTableBodyMixin from  '../mixins/two-col-table-body-mixin';

export default Component.extend(TwoColTableBodyMixin, {
});
